﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Concurrent;
using System.IO;
using Newtonsoft.Json;
using BP.En;
using BP.Port;
using BP.DA;
using BP.Sys;
using BP.WF;

namespace CCFlow
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BP.Sys.SystemConfig.CCBPMRunModel == CCBPMRunModel.SAAS)
            {
                this.Response.Redirect("Default.htm");
                return;
            }
        }
    }
}